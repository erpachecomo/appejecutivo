import { NgModule, ErrorHandler } from '@angular/core';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { HomePage } from '../pages/home/home';
import { ReservationPage } from '../pages/reservation/reservation';
import { RoomServicePage } from '../pages/room-service/room-service';
import { AboutPage } from '../pages/about/about';
import { PlacesPage } from '../pages/places/places';
import { WeatherPage } from '../pages/weather/weather';

import { Http } from '@angular/http';
import { 
  TranslateModule, 
  TranslateStaticLoader, 
  TranslateLoader } from 'ng2-translate/ng2-translate';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ReservationPage,
    RoomServicePage,
    WeatherPage,
    AboutPage,
    PlacesPage
  ],
  imports: [
    IonicModule.forRoot(MyApp),
  TranslateModule.forRoot({
    provide: TranslateLoader,
    useFactory: (createTranslateLoader),
    deps: [Http]
  })
],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
        HomePage,
    ReservationPage,
        RoomServicePage,
    WeatherPage,
    AboutPage,
    PlacesPage
  ],
  providers: [{provide: ErrorHandler, useClass: IonicErrorHandler}]
})
export class AppModule {

  
}
export function createTranslateLoader(http: Http) {
  return new TranslateStaticLoader(http, 'assets/i18n', '.json');
}
