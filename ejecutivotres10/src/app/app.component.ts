import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar, Splashscreen } from 'ionic-native';
import { TranslateService } from 'ng2-translate';

import { HomePage } from '../pages/home/home';
import { ReservationPage } from '../pages/reservation/reservation';
import { RoomServicePage } from '../pages/room-service/room-service';
import { AboutPage } from '../pages/about/about';
import { WeatherPage } from '../pages/weather/weather';
import { PlacesPage } from '../pages/places/places';





@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = HomePage;
translate: TranslateService;
  pages: Array<{title: string, component: any}>;

  constructor(public platform: Platform,translate: TranslateService) {
    this.translate=translate;
  this.translate.setDefaultLang('es');    
    this.initializeApp();

    this.pages = [
      { title: 'verhotel', component: HomePage },
      { title: 'roomservice', component: RoomServicePage },      
      { title: 'reservar', component: ReservationPage },
      { title: 'PLACES', component: PlacesPage },
      { title: 'WEATHER', component: WeatherPage },
      { title: 'ABOUT', component: AboutPage }
    ];

  }

  initializeApp() {

    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      StatusBar.styleDefault();
      Splashscreen.hide();
    });
  }
  changeLanguage(){
    console.log(this.translate.currentLang);
    if(this.translate.currentLang==='es'){
      this.translate.use('en');
    }else{
      this.translate.use('es');
    }
  }
  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }
}
